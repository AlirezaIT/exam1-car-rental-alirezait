# Exam #1: "Car Rental"

## Student: s269967 ASHTARI ALIREZA

## Initialize DataBase

Please Run these command:

- node_modules/.bin/sequelize-cli db:migrate:all
- node_modules/.bin/sequelize-cli db:seed:all

## React client application routes

- Route `/`: redirected to /vehicles
- Route `/vehicles`: Showing the list of all vahicles and their brands and models
- Route `/configure`: Main configure page
- Route `/payment`: Payment Page
- Route `/not-found`: Not found page
- Route `/login`: login page
- Route `/logout`: for logging out

## REST API server

- POST `/login`
  - request login and send username/password
  - response confirmation
- POST `/loghout`
  - sending request
  - response confirmation
- GET `/vehicles`
  - response the liste of all vehicles
- GET `/reservations`
  - response the liste of all reservation
- GET `/history`
  - response the liste of all history
- GET `/user`
  - response the isAuthenticated user
- GET `/vehicles`
  - response the liste of all vehicles
- POST `/all_vehicles_in_specific_category`
  - request the list of vehicles in sepecific category
  - response the number of all of them
- POST `/category_price`
  - request the price of specific category
  - response the price
- POST `/reservation_between_date`
  - request the number of reserved car based on category
  - response the number of all of them
- POST `/vehicle_booked`
  - request to store and reserv the vehicles
  - response confirmation and storing into the database
- POST `'/cancel_reservation'`
  - request to remove from db/list of reservations
  - response confirmation

## Server database

- Table `users` - contains xx yy zz
- Table `vehicles` - list of all vehicles and their brand & model & category and category's price
- Table `reservations` - contains the details of all reservation (current and pass which is flaged by status)

## Main React Components

- `Vehicles` (in `App.js`): contain the data of main page
- `configure` (in `App.js`): cotains the data and arrange the configure page and and includes the components (form.jsx , reservations.jsx,history.jsx)
- `Criteria` (in `configure.jsx`): contains the form and also calculating the price and searching
- `Login` (in `App.js`): handling the login page and operation -
- `Payment` (in `App.js`): handling the payment and saving the reservation.

(only _main_ components, minor ones may be skipped)

## Screenshot

![Configurator Screenshot](https://www.mediafire.com/convkey/fd47/7a1mnmsh6mck8vz6g.jpg)

## Test users

- username ,password
- admin, 123
- alireza, 123
- mosi, 123
- farhad, 123
- amir, 123
- token secret key is 12345
